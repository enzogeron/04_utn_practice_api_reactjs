import React from 'react';
import { CreateBook, ListBook } from '../../components/book';

const BookPage = () => {

    return (
        <div className="row">
            <div className="col-md-5">
                <p><strong>Agregar nuevo</strong></p>
                <CreateBook />
            </div>
            <div className="col-md-7">
                <p><strong>Listado de libros</strong></p>
                <ListBook />
            </div>
        </div>
    );
}

export default BookPage;
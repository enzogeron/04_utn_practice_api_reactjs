import React from 'react';
import { CreatePerson, ListPerson } from '../../components/person'

const PersonPage = () => {
    return (
        <div className="row">
            <div className="col-md-5">
                <p><strong>Agregar nuevo</strong></p>
                <CreatePerson />
            </div>
            <div className="col-md-7">
                <p><strong>Listado de Amigos</strong></p>
                <ListPerson />
            </div>
        </div>
    );
}

export default PersonPage;
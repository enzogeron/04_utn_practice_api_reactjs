import React from 'react';
import { Switch, Route, Link } from 'react-router-dom';

import { ListBook, EditBook, LendBook } from '../components/book';
import { ListCategory, FormCategory, BooksByCategory } from '../components/category';
import { EditPerson, BooksByPerson } from '../components/person';
import BookPage from './book';
import PersonPage from './person/';
import HomePage from './home';

const Menu = () => {

    return (
        <>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <a href="/" className="navbar-brand">ReactJS</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-reactjs" aria-controls="navbar-reactjs" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbar-reactjs">
                    <ul className="navbar-nav">

                        <li className="nav-item dropdown">
                            <Link to={'/libros'} className="nav-link">
                                Libros
                            </Link>
                            {/* <div class="dropdown-menu" aria-labelledby="navbarDropdownBooks">
                                <Link to={'/libros'} className="dropdown-item">Listado</Link>
                                <Link to={'/libros/crear'} className="dropdown-item">Crear</Link>
                            </div> */}
                        </li>

                        <li className="nav-item dropdown">
                            <a href="#" className="nav-link dropdown-toggle" id="navbarDropdownCategories" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Categorias
                        </a>
                            <div className="dropdown-menu" aria-labelledby="navbarDropdownCategories">
                                <Link to={'/categorias'} className="dropdown-item">Listado</Link>
                                <Link to={'/categorias/crear'} className="dropdown-item">Crear</Link>
                            </div>
                        </li>

                        <li className="nav-item dropdown">
                            <Link to={'/personas'} className="nav-link">
                                Amigos
                            </Link>
                            {/* <div class="dropdown-menu" aria-labelledby="navbarDropdownPersons">
                                <Link to={'/personas'} className="dropdown-item">Ver listado</Link>
                                <Link to={'/personas/crear'} className="dropdown-item">Crear</Link>
                            </div> */}
                        </li>

                    </ul>
                </div>
            </nav>

            <div className="container mt-3">
                <Switch>
                    <Route exact path="/" component={HomePage} />
                    <Route exact path="/personas" component={PersonPage} />
                    <Route path="/personas/editar/:id" component={EditPerson} />
                    <Route path="/libros/persona/:id" component={BooksByPerson} />
                    <Route exact path="/categorias" component={ListCategory} />
                    <Route exact path="/categorias/crear" component={FormCategory} />
                    <Route path="/libros/categoria/:id" component={BooksByCategory} />
                    <Route exact path="/libros" component={BookPage} />
                    <Route path="/libros/editar/:id" component={EditBook} />
                    <Route path="/libros/prestar/:id" component={LendBook} />
                    <Route path="*" component={() => <div>404</div>} />
                </Switch>
            </div>
        </>
    );
}

export default Menu;
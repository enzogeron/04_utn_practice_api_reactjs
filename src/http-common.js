import axios from 'axios';

export default axios.create({
    baseURL: 'https://utn-nodejs-app.herokuapp.com/',
    headers: {
        'Content-type': 'application/json'
    }
});
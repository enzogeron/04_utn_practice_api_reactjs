import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { ToastProvider } from 'react-toast-notifications';
import 'bootstrap/dist/css/bootstrap.min.css';
import Menu from './pages/Menu';
import { PersonProvider } from './context/PersonContext';
import { BookProvider } from './context/BookContext';
import { CategoryProvider } from './context/CategoryContext';
import './index.css';

ReactDOM.render(
  <ToastProvider>
    <BookProvider>
      <PersonProvider>
        <CategoryProvider>
          <BrowserRouter>
            <Menu />
          </BrowserRouter>
        </CategoryProvider>
      </PersonProvider >
    </BookProvider>
  </ToastProvider >,
  document.getElementById('app')
);

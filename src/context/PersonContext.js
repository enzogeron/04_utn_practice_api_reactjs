import React, { createContext, useState } from 'react';
import { getAllPersons, createPerson, deletePersonById, getPersonById, updatePersonById, getAllBooks } from '../services/api';
import { useToasts } from 'react-toast-notifications';

export const PersonContext = createContext();

export const PersonProvider = ({ children }) => {

    const { addToast } = useToasts();

    const initialStatePerson = {
        nombre: '',
        apellido: '',
        alias: '',
        email: '',
    };

    // Amigo
    const [person, setPerson] = useState(initialStatePerson);
    // Listado de amigos
    const [persons, setPersons] = useState([]);
    // Libros prestados a un amigo
    const [booksByPerson, setBooksByPerson] = useState([]);

    const getPersons = async () => {
        try {
            const { status, data } = await getAllPersons();
            if (status === 200) {
                setPersons(data);
            }
        } catch (error) {
            console.log(error)
        }
    }

    const getPerson = async (id) => {
        try {
            const { status, data } = await getPersonById(id);
            if (status === 200) {
                setPerson(data);
            }
        } catch (error) {
            console.log(error);
        }
    }

    const addPerson = async () => {
        let message = '';
        let appearance = '';
        try {
            const { status, data } = await createPerson(person);
            if (status === 200) {
                message = `Se creo correctamente el usuario ${data.email}`;
                appearance = 'success';
                setPerson(initialStatePerson);
                setPersons([data, ...persons]);
            }
        } catch (error) {
            message = error.response.data.mensaje || 'error';
            appearance = 'error';
        }
        addToast(message, { appearance: appearance, autoDismiss: true });
    }

    const updatePerson = async (id) => {
        let message = '';
        let appearance = '';
        try {
            const { status } = await updatePersonById(id, person);
            if (status === 200) {
                message = 'Se actualizaron los datos de la persona.';
                appearance = 'success';
            }
        } catch (error) {
            message = error.response.data.mensaje || 'Error al actualizar los datos.';
            appearance = 'error';
        }
        addToast(message, { appearance: appearance, autoDismiss: true });
    }

    const deletePerson = async (id) => {
        let message = '';
        let appearance = '';
        try {
            const { status, data } = await deletePersonById(id);
            if (status === 200) {
                message = data.mensaje;
                appearance = 'success';
                setPersons(persons.filter(person => person.id !== id));
            }
        } catch (error) {
            message = error.response.data.mensaje || 'Error al eliminar la persona.';
            appearance = 'error';
            console.log(error);
        }
        addToast(message, { appearance: appearance, autoDismiss: true });
    }

    const getBooksByPerson = async (id) => {
        try {
            const { status, data } = await getAllBooks();
            if (status === 200) {
                console.log(data.filter(b => b.persona_id === parseInt(id)));
                setBooksByPerson(data.filter(b => b.persona_id === parseInt(id)));
                // setBooksByPerson(data);
            }
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <PersonContext.Provider value={{
            person,
            setPerson,
            getPerson,
            persons,
            getPersons,
            setPersons,
            addPerson,
            updatePerson,
            deletePerson,
            booksByPerson,
            getBooksByPerson,
        }}>
            {children}
        </PersonContext.Provider>
    );
}

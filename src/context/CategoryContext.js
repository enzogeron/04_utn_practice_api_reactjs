import React, { createContext, useState } from 'react';
import { getAllCategories, getAllBooks } from '../services/api';

export const CategoryContext = createContext();

export const CategoryProvider = ({ children }) => {

    // Lista de categorias
    const [categories, setCategories] = useState([]);
    // Libros por categoria
    const [booksByCategory, setBooksByCategory] = useState([]);

    const getCategories = async () => {
        try {
            const { status, data } = await getAllCategories();
            if (status === 200) {
                setCategories(data);
            }
        } catch (error) {
            console.log(error);
        }
    }

    const getBooksByCategory = async (id) => {
        try {
            const { status, data } = await getAllBooks();
            if (status === 200) {
                console.log(data.filter(b => b.categoria_id === parseInt(id)));
                setBooksByCategory(data.filter(b => b.categoria_id === parseInt(id)));
            }
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <CategoryContext.Provider value={{
            categories,
            getCategories,
            booksByCategory,
            getBooksByCategory,
        }}>
            { children}
        </CategoryContext.Provider>
    );
}
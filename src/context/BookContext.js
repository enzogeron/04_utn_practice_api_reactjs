import React, { createContext, useState } from 'react';
import { getAllBooks, createBook, getBookById, deleteBookById, updateBookById, lendBookById, returnBookById } from '../services/api';
import { useToasts } from 'react-toast-notifications';

export const BookContext = createContext();

export const BookProvider = ({ children }) => {

    const { addToast } = useToasts();

    const initialStateBook = {
        nombre: '',
        descripcion: '',
        categoria_id: '',
        persona_id: '',
    };

    // Libro
    const [book, setBook] = useState(initialStateBook);
    // Lista de libros
    const [books, setBooks] = useState([]);

    const getBooks = async () => {
        try {
            const { status, data } = await getAllBooks();
            if (status === 200) {
                setBooks(data);
            }
        } catch (error) {
            console.log(error);
        }
    }

    const getBook = async (id) => {
        try {
            const { status, data } = await getBookById(id);
            if (status === 200) {
                setBook(data);
            }
        } catch (error) {
            console.log(error);
        }
    }

    const addBook = async () => {
        let message = '';
        let appearance = '';
        try {
            const { status, data } = await createBook(book);
            if (status === 200) {
                message = `Se creo correctamente el libro ${data.nombre}`;
                appearance = 'success';
                setBook(initialStateBook);
                setBooks([data, ...books]);
            }
        } catch (error) {
            message = error.response.data.mensaje || 'error';
            appearance = 'error';
        }
        addToast(message, { appearance: appearance, autoDismiss: true });
    }

    const updateBook = async (id) => {
        let message = '';
        let appearance = '';
        try {
            const { status } = await updateBookById(id, book);
            if (status === 200) {
                message = 'Se actualizaron los datos del libro.';
                appearance = 'success';
            }
        } catch (error) {
            message = error.response.data.mensaje || 'Error al actualizar los datos.';
            appearance = 'error';
        }
        addToast(message, { appearance: appearance, autoDismiss: true });
    }

    const deleteBook = async (id) => {
        let message = '';
        let appearance = '';
        try {
            const { status, data } = await deleteBookById(id);
            if (status === 200) {
                message = data.mensaje;
                appearance = 'success';
                setBooks(books.filter(person => person.id !== id));
            }
        } catch (error) {
            message = error.response.data.mensaje || 'Error al eliminar el libro.';
            appearance = 'error';
            console.log(error);
        }
        addToast(message, { appearance: appearance, autoDismiss: true });
    }

    const lendBook = async (id, lendData) => {
        let message = '';
        let appearance = '';
        try {
            const { status, data } = await lendBookById(id, lendData);
            if (status === 200) {
                message = data.message;
                appearance = 'success';
            }
        } catch (error) {
            message = error.response.data.mensaje || 'Error al prestar el libro.';
            appearance = 'error';
            console.log(error);
        }
        addToast(message, { appearance: appearance, autoDismiss: true });
    }

    const returnBook = async (id) => {
        let message = '';
        let appearance = '';
        try {
            const { status, data } = await returnBookById(id);
            if (status === 200) {
                message = data.mensaje;
                appearance = 'success';
                const book = books.find((b) => b.id === id);
                book.persona_id = null;
            }
        } catch (error) {
            message = error.response.data.mensaje || 'Error al devolver el libro.';
            appearance = 'error';
            console.log(error);
        }
        addToast(message, { appearance: appearance, autoDismiss: true });
    }

    return (
        <BookContext.Provider value={{
            book,
            setBook,
            getBook,
            books,
            getBooks,
            addBook,
            updateBook,
            deleteBook,
            lendBook,
            returnBook,
        }}>
            { children}
        </BookContext.Provider>
    );
}
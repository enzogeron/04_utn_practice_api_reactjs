import React, { useContext, useEffect } from 'react';
import { PersonContext } from '../../context/PersonContext';

import CardPerson from './CardPerson';

const ListPerson = () => {

    const { persons, getPersons } = useContext(PersonContext);

    useEffect(() => {
        getPersons();
    }, []);

    return (
        <>
            { persons.map(person => <CardPerson key={person.id} person={person} />)}
        </>
    );
}

export default ListPerson;
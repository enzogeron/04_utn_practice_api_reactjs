import React, { useContext, useEffect } from 'react';
import { PersonContext } from '../../context/PersonContext';

import FormPerson from './FormPerson';

const EditPerson = ({ match }) => {

    const { id } = match.params;

    const { person, getPerson, setPerson, updatePerson } = useContext(PersonContext);

    useEffect(() => {
        getPerson(id);
    }, []);

    return (
        <div className="col-6 mx-auto">
            <FormPerson person={person} setPerson={setPerson} action={'update'} method={updatePerson} />
        </div>
    );
}

export default EditPerson;
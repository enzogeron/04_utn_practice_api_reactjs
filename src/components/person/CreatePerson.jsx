import React, { useContext } from 'react';
import { PersonContext } from '../../context/PersonContext';

import FormPerson from './FormPerson';

const CreatePerson = () => {

    const { person, setPerson, addPerson } = useContext(PersonContext);

    return (
        <FormPerson person={person} setPerson={setPerson} action={'create'} method={addPerson} />
    );
}

export default CreatePerson;
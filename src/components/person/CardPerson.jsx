import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { PersonContext } from '../../context/PersonContext';

const CardPerson = ({ person }) => {

    const history = useHistory();

    const { deletePerson } = useContext(PersonContext);

    const editPerson = (id) => {
        history.push(`/personas/editar/${id}`);
    }

    const getBooks = (id) => {
        history.push(`/libros/persona/${id}`);
    }

    return (
        <div className="card-item-list">
            <div>
                <small>ID: {person.id}</small> - {person.nombre} {person.apellido}
            </div>
            <div className="actions">
                <button className="btn btn-sm btn-info" onClick={() => editPerson(person.id)}>Editar</button>
                <button className="btn btn-sm btn-danger" onClick={() => deletePerson(person.id)}>X</button>
                <button className="btn btn-sm btn-light" onClick={() => getBooks(person.id)}>Ver libros</button>
            </div>
        </div>
    );

}

export default CardPerson;
import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';

const FormPerson = ({ person, setPerson, action, method }) => {

    const history = useHistory();

    const initialStatePerson = {
        nombre: '',
        apellido: '',
        alias: '',
        email: '',
    };

    const handleChange = (e) => {
        const { name, value } = e.target;
        setPerson({ ...person, [name]: value });
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        action === 'update' ? method(person.id) : method();
    }

    const backPage = () => {
        history.goBack();
    }

    useEffect(() => {
        action === 'create' && setPerson(initialStatePerson);
    }, []);

    return (
        <form className="card card-body" onSubmit={(handleSubmit)}>
            <div className="form-group">
                <label htmlFor="nombre">Nombre:</label>
                <input
                    type="text"
                    name="nombre"
                    className="form-control"
                    maxLength="50"
                    onChange={handleChange}
                    value={person.nombre}
                />
            </div>

            <div className="form-group">
                <label htmlFor="apellido">Apellido:</label>
                <input
                    type="text"
                    name="apellido"
                    className="form-control"
                    maxLength="50"
                    onChange={handleChange}
                    value={person.apellido}
                />
            </div>

            <div className="form-group">
                <label htmlFor="alias">Alias:</label>
                <input
                    type="text"
                    name="alias"
                    className="form-control"
                    onChange={handleChange}
                    value={person.alias}
                />
            </div>

            <div className="form-group">
                <label htmlFor="email">Email:</label>
                <input
                    type="text"
                    name="email"
                    className="form-control"
                    onChange={handleChange}
                    value={person.email}
                />
            </div>

            { action === 'update' && <a className="btn btn-dark btn-block" onClick={() => backPage()}>Volver</a>}

            <button className="btn btn-primary btn-block">
                Enviar
            </button>

        </form>
    );

}

export default FormPerson;
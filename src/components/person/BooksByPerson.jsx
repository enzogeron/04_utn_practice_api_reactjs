import React, { useContext, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { PersonContext } from '../../context/PersonContext';

const BooksByPerson = ({ match }) => {

    const { id } = match.params;

    const history = useHistory();

    const { person, getPerson, booksByPerson, getBooksByPerson } = useContext(PersonContext);

    const backPage = () => {
        history.goBack();
    }
    useEffect(() => {
        getBooksByPerson(id);
        getPerson(id);
    }, []);

    return (
        <div className="col-6 mx-auto">
            { <h5> {booksByPerson.length === 0 ? 'No hay libros prestados para:' : 'Libros prestados a'} {person.nombre} {person.apellido}</h5>}
            { booksByPerson.map(book => <h5>{book.nombre}</h5>)}
            <a className="btn btn-dark btn-block" onClick={() => backPage()}>Volver</a>
        </div>
    );
}

export default BooksByPerson;
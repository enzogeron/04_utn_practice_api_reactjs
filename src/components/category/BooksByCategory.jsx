import React, { useContext, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { CategoryContext } from '../../context/CategoryContext';

const BooksByCategory = ({ match }) => {

    const { id } = match.params;

    const history = useHistory();

    const { booksByCategory, getBooksByCategory } = useContext(CategoryContext);

    const backPage = () => {
        history.goBack();
    }
    useEffect(() => {
        getBooksByCategory(id);
    }, []);

    return (
        <div className="col-6 mx-auto">
            { booksByCategory.length === 0 && <h5>No hay libros en esta categoria.</h5>}
            { booksByCategory.length > 0 &&
                // <h5>Libros prestados a {person.nombre} {person.apellido}</h5>
                booksByCategory.map(book => <h5>{book.nombre}</h5>)
            }
            <a className="btn btn-dark btn-block" onClick={() => backPage()}>Volver</a>
        </div>
    );
}

export default BooksByCategory;
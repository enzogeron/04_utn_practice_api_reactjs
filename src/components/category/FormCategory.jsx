import React, { useState } from 'react';
import { createCategory } from '../../services/api'

import { useToasts } from 'react-toast-notifications';

const FormCategory = () => {

    const { addToast } = useToasts();

    const initialFormState = {
        nombre: '',
    }

    const [form, setForm] = useState(initialFormState)

    const handleFormChange = (event) => {
        const { name, value } = event.target;
        setForm({ ...form, [name]: value });
    }

    const saveForm = async (e) => {
        e.preventDefault();
        let message = '';
        let appearance = '';
        try {
            const { status, data } = await createCategory(form);

            if (status === 200) {
                message = `Se creo correctamente la categoria ${data.nombre}.`;
                appearance = 'success';
                setForm(initialFormState);
            }

        } catch (error) {
            message = error.response.data.mensaje || 'No fue posible crear la categoria.';
            appearance = 'error';
        }
        addToast(message, { appearance: appearance, autoDismiss: true })
    }

    return (
        <form onSubmit={saveForm} className="offset-1 col-10">
            <div className="form-group">
                <label htmlFor="titulo">Titulo</label>
                <input
                    type="text"
                    className="form-control"
                    id="nombre"
                    name="nombre"
                    value={form.nombre}
                    onChange={handleFormChange}
                />
            </div>
            <button type="submit" className="btn btn-primary">Enviar</button>
        </form>
    );
}

export default FormCategory;
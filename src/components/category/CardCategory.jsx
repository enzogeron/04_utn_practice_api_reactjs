import React from 'react';
import { Link } from 'react-router-dom';

const CardCategory = ({ id, name, deleteCategory }) => {

    return (
        <div key={id} className="card">
            <div className="card-body">
                <h5 className="card-title">{name}</h5>
                <Link
                    to={`/libros/categoria/${id}`}
                    className="btn btn-sm btn-info ml-2"
                >
                    Ver libros
                </Link>
                <button onClick={() => deleteCategory(id)} className="btn btn-sm btn-danger ml-2">Delete</button>
            </div>
        </div>
    );
}

export default CardCategory;
import React, { useState, useEffect } from 'react';
import CardCategory from './CardCategory';
import { getAllCategories, deleteCategoryById } from '../../services/api';
import { useToasts } from 'react-toast-notifications';

const ListCategory = () => {

    const { addToast } = useToasts();

    const [categories, setCategories] = useState([]);

    useEffect(() => {
        getCategories();
    }, []);

    const getCategories = async () => {
        try {
            const { status, data } = await getAllCategories();

            if (status === 200) {
                const categories = data;
                setCategories(categories);
            }

        } catch (error) {
            console.log(error)
        }

    }

    const deleteCategory = async (id) => {
        let message = '';
        let appearance = '';
        try {
            const { status, data } = await deleteCategoryById(id);
            if (status === 200) {
                message = data.mensaje;
                appearance = 'success';
                setCategories(categories.filter(category => id !== category.id));
            }
        } catch (error) {
            message = error.response.data.mensaje || 'No fue posible borrar la categoria.';
            appearance = 'error';
        }
        addToast(message, { appearance: appearance, autoDismiss: true })
    }

    return (
        <div>
            {categories.map(category => <CardCategory id={category.id} name={category.nombre} deleteCategory={deleteCategory} />)}
        </div>
    )
};

export default ListCategory;
import React, { useContext, useEffect } from 'react';
import { BookContext } from '../../context/BookContext';

import FormBook from './FormBook';

const EditBook = ({ match }) => {

    const { id } = match.params;

    const { book, getBook, setBook, updateBook } = useContext(BookContext);

    useEffect(() => {
        getBook(id);
    }, []);

    return (
        <div className="col-6 mx-auto">
            <FormBook book={book} setBook={setBook} action={'update'} method={updateBook} />
        </div>
    );
}

export default EditBook;
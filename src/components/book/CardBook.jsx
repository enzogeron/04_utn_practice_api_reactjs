import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { BookContext } from '../../context/BookContext';

const CardPerson = ({ book }) => {

    const history = useHistory();

    const { deleteBook, returnBook } = useContext(BookContext);

    const editBook = (id) => {
        history.push(`/libros/editar/${id}`);
    }

    const lendBook = (id) => {
        history.push(`/libros/prestar/${id}`);
    }

    return (
        <div className="card-item-list">
            <div>
                <small>ID: {book.id}</small> - {book.nombre} |
            </div>
            <div className="actions">
                {
                    book.persona_id ?
                        <button className="btn btn-sm btn-link" onClick={() => returnBook(book.id)}>Devolver</button> :
                        <button className="btn btn-sm btn-link" onClick={() => lendBook(book.id)}>Prestar</button>
                }
                <button className="btn btn-sm btn-info" onClick={() => editBook(book.id)}>Editar</button>
                <button className="btn btn-sm btn-danger" onClick={() => deleteBook(book.id)}>X</button>
            </div>
        </div>
    );

}

export default CardPerson;
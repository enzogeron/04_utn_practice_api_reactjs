import React, { useState, useContext, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { PersonContext } from '../../context/PersonContext';
import { BookContext } from '../../context/BookContext';

const LendBook = ({ match }) => {

    const { id } = match.params;

    const initialStateLendBook = {
        persona_id: '',
    }
    const history = useHistory();

    const [lendBook, setLendBook] = useState(initialStateLendBook);

    const { persons, getPersons } = useContext(PersonContext);
    const { book, getBook, lendBook: lendBookById } = useContext(BookContext);

    const handleChange = (e) => {
        const { name, value } = e.target;
        setLendBook({ ...lendBook, [name]: value });
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        lendBookById(id, lendBook)
    }

    const backPage = () => {
        history.goBack();
    }

    useEffect(() => {
        getPersons();
        getBook(id);
    }, []);

    return (
        <form className="col-5 mx-auto" onSubmit={handleSubmit}>
            <h6>Prestar el libro: <strong>{book.nombre}</strong></h6>
            <div className="form-group">
                <label htmlFor="persona_id">Amigo</label>
                <select name="persona_id" id="persona_id" className="form-control" onChange={handleChange} value={lendBook.persona_id}>
                    <option value="" selected disabled hidden>Seleccion un amigo</option>
                    {persons.map((person) => <option value={person.id}>{person.nombre} {person.apellido}</option>)}
                </select>
            </div>

            <a className="btn btn-dark btn-block" onClick={() => backPage()}>Volver</a>

            <button className="btn btn-primary btn-block">
                Prestar
            </button>
        </form>
    );
}

export default LendBook;
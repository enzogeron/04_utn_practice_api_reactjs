import React, { useContext } from 'react';
import { BookContext } from '../../context/BookContext';

import FormBook from './FormBook';

const CreateBook = () => {

    const { book, setBook, addBook } = useContext(BookContext);

    return (
        <FormBook book={book} setBook={setBook} action={'create'} method={addBook} />
    );
}

export default CreateBook;
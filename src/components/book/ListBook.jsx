import React, { useEffect, useContext } from 'react';
import { BookContext } from '../../context/BookContext';
import CardBook from './CardBook';

const ListBook = () => {

    const { books, getBooks } = useContext(BookContext);

    useEffect(() => {
        getBooks()
    }, []);

    // const getBooks = async () => {
    //     const response = await getAllBooks();
    //     (!category_id && !person_id) && setBooks(response.data);
    //     category_id && setBooks(response.data.filter(book => parseInt(category_id) === book.categoria_id));
    //     person_id && setBooks(response.data.filter(person => parseInt(person_id) === person.persona_id));
    // }

    return (
        <div>
            {books.map((book) => <CardBook key={book.id} book={book} />)}
        </div>
    );
}

export default ListBook;
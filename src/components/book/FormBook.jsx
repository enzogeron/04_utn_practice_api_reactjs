import React, { useContext, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

import { PersonContext } from '../../context/PersonContext';
import { CategoryContext } from '../../context/CategoryContext';

const FormBook = ({ book, setBook, action, method }) => {

    const initialStateBook = {
        nombre: '',
        descripcion: '',
        categoria_id: '',
        persona_id: '',
    };

    const { persons, getPersons } = useContext(PersonContext);
    const { categories, getCategories } = useContext(CategoryContext);

    const history = useHistory();

    const handleSubmit = (e) => {
        e.preventDefault();
        action === 'update' ? method(book.id) : method();
    }

    const handleChange = (e) => {
        const { name, value } = e.target;
        setBook({ ...book, [name]: value });
    }

    const backPage = () => {
        history.goBack();
    }

    useEffect(() => {
        getPersons();
        getCategories();
        action === 'create' && setBook(initialStateBook);
    }, []);

    return (
        <form className="card card-body" onSubmit={handleSubmit}>

            <div className="form-group">
                <label htmlFor="nombre">Nombre:</label>
                <input
                    type="text"
                    name="nombre"
                    maxLength="50"
                    className="form-control"
                    onChange={handleChange}
                    value={book.nombre}
                />
            </div>

            <div className="form-group">
                <label htmlFor="apellido">Descripcion</label>
                <textarea name="descripcion" id="descripcion" cols="30" rows="4" className="form-control" onChange={handleChange} value={book.descripcion}></textarea>
            </div>

            <div className="form-group">
                <label htmlFor="categoria_id">Categoria</label>
                <select name="categoria_id" id="categoria_id" className="form-control" onChange={handleChange} value={book.categoria_id}>
                    <option value="" selected disabled hidden>Seleccion una categoria</option>
                    {categories.map((category) => <option value={category.id}>{category.nombre}</option>)}
                    <option value="1254512" >Seleccion una dasdas</option>
                </select>
            </div>

            <div className="form-group">
                <label htmlFor="persona_id">Amigo</label>
                <select name="persona_id" id="persona_id" className="form-control" onChange={handleChange} value={book.persona_id}>
                    <option value="" selected disabled hidden>Seleccion un amigo</option>
                    {persons.map((person) => <option value={person.id}>{person.nombre} {person.apellido}</option>)}
                </select>
            </div>

            { action === 'update' && <a className="btn btn-dark btn-block" onClick={() => backPage()}>Volver</a>}

            <button className="btn btn-primary btn-block">
                Enviar
            </button>

        </form>
    );
}

export default FormBook;
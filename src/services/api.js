import http from '../http-common';


/**
 * Categorias
 */
const createCategory = (data) => {
    return http.post(`/categoria`, data);
}

const getAllCategories = () => {
    return http.get(`/categoria`);
}

const getCategoryById = (id) => {
    return http.get(`/categoria/${id}`);
}

const deleteCategoryById = (id) => {
    return http.delete(`/categoria/${id}`);
}

/**
 * Personas
 */
const createPerson = (data) => {
    return http.post(`/persona`, data);
}

const getAllPersons = () => {
    return http.get(`/persona`);
}

const getPersonById = (id) => {
    return http.get(`/persona/${id}`);
}

const updatePersonById = (id, data) => {
    return http.put(`/persona/${id}`, data);
}

const deletePersonById = (id) => {
    return http.delete(`/persona/${id}`);
}

/**
 * Libros
 */
const createBook = (data) => {
    return http.post(`/libro`, data);
}

const getAllBooks = () => {
    return http.get(`/libro`);
}

const getBookById = (id) => {
    return http.get(`/libro/${id}`);
}

const updateBookById = (id, data) => {
    return http.put(`/libro/${id}`, data);
}

const deleteBookById = (id) => {
    return http.delete(`/libro/${id}`);
}

// prestar libro
const lendBookById = (id, data) => {
    return http.put(`/libro/prestar/${id}`, data);
}

// devolver libro
const returnBookById = (id) => {
    return http.put(`/libro/devolver/${id}`);
}

export {
    createCategory,
    getAllCategories,
    getCategoryById,
    deleteCategoryById,
    createPerson,
    getAllPersons,
    getPersonById,
    updatePersonById,
    deletePersonById,
    createBook,
    getAllBooks,
    getBookById,
    updateBookById,
    deleteBookById,
    lendBookById,
    returnBookById,
};